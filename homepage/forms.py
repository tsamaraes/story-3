from django import forms
from .models import Friends, ClassYear

class FriendForm(forms.ModelForm):
    
    class Meta:
        model = Friends
        fields = ['name', 
        'ClassYear', 
        'hobby', 
        'food', 
        'drink']
        labels = {'name': 'Name',
        'ClassYear': 'Batch',
        'hobby': 'Hobby',
        'food': 'Favorite Food',
        'drink': 'Favorite Drink',
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].widget.attrs.update({
                'class': 'form-control'
            })

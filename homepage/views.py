from django.shortcuts import render
from django.shortcuts import redirect
from .models import Friends, ClassYear
from . import forms

# Create your views here.
def story3(request):
    return render(request, 'story3.html')

def profilepage(request):
    return render(request, 'profilepage.html')

def experience(request):
    return render(request, 'experience.html')

def portfolio(request):
    return render(request, 'portfolio.html')

def seefriend(request):
    friends = Friends.objects.all()
    classyear = ClassYear.objects.all()
    response = {'friends' : friends, 'classyear': classyear}
    return render(request, 'seefriend.html', response)

def addfriend(request):
    if request.method == 'POST':
        form = forms.FriendForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('homepage:seefriend')
    else:
        form = forms.FriendForm()
    return render(request, 'addfriend.html', {'form': form})

def deletefriend(request, name):
    friends = Friends.objects.get(name=name)
    friends.delete()
    return redirect('/seefriend/')
from django.db import models
from django.forms import ModelForm

#Create your models here.
class ClassYear(models.Model):
    BATCH_CHOICES= [('Maung (2019)', 'Maung (2019)'),
    ('Quanta (2018)', 'Quanta (2018)'),
    ('Tarung (2017)', 'Tarung (2017)'),
    ('Omega (2016)', 'Omega (2016)'),
    ('Others', 'Others')]
    batch = models.CharField(max_length=255, choices=BATCH_CHOICES, default='NULL')

    def __str__(self):
        return self.batch

class Friends(models.Model):
    name = models.CharField(max_length=255, primary_key=True)
    ClassYear = models.ForeignKey(ClassYear, on_delete=models.CASCADE, default='Fasilkom')
    hobby = models.CharField(max_length=255)
    food = models.CharField(max_length=255)
    drink = models.CharField(max_length=255)


    def __str__(self):
        return self.name


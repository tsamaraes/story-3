# Generated by Django 3.0.3 on 2020-02-23 06:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0004_auto_20200223_0643'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='friends',
            name='batch',
        ),
        migrations.AddField(
            model_name='classyear',
            name='friends',
            field=models.ForeignKey(default='Anonymous', on_delete=django.db.models.deletion.CASCADE, to='homepage.Friends'),
        ),
    ]

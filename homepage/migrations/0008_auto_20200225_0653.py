# Generated by Django 3.0.3 on 2020-02-25 06:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('homepage', '0007_auto_20200225_0641'),
    ]

    operations = [
        migrations.AlterField(
            model_name='friends',
            name='drink',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='friends',
            name='food',
            field=models.CharField(max_length=255),
        ),
        migrations.AlterField(
            model_name='friends',
            name='hobby',
            field=models.CharField(max_length=255),
        ),
    ]

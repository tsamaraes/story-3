from django.urls import path
from . import views
from django.contrib import admin

app_name = 'homepage'

urlpatterns = [
    path('', views.story3, name='story3'),
    path('profilepage/', views.profilepage, name='profilepage'),
    path('experience/', views.experience, name='experience'),
    path('portfolio/', views.portfolio, name='portfolio'),
    path('seefriend/', views.seefriend, name='seefriend'),
    path('addfriend/', views.addfriend, name='addfriend'),
    path('seefriend/deletefriend/<str:name>', views.deletefriend, name='deletefriend'),
    # dilanjutkan ...
]